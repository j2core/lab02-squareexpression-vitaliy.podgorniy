package com.j2core.vpodgorny.week02.QuadraticEquation;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.Math.sqrt;

/**
 * Created by vip on 24.05.2016.
 * QuadraticEquation class for calculating equation ax^2 + bx + c = 0
 * Example1 teo roots: x^2+3x-4 = 0 printing: The equation has two roots x1=  1.0 and x2 = -4.0
 * Example2 one root: x^2+6x+9 = 0 printing: The equation has one root x1= -3.0
 * Example3 no roots: x^2+x+1 = 0 printing: The equation hasn't roots
 */
public class QuadraticEquation {
    private static final double ADMISSIBLE_ERROR = 0.00000001;
    private static boolean isExit;

    public static void main(String[] args) {
        double a = 0, b = 0, c = 0;
        System.out.println("Insert number for parametr or input q for exit");
        a = readConsole();
        System.out.println("a=" + a);
        b = readConsole();
        System.out.println("b=" + b);
        c = readConsole();
        System.out.println("c=" + c);
        double discriminant = (b * b) - (4 * a * c);
        System.out.println("D = " + discriminant);
        if (discriminant > ADMISSIBLE_ERROR) {
            double x1 = ((-b + sqrt(discriminant)) / (2 * a));
            double x2 = ((-b - sqrt(discriminant)) / (2 * a));
            System.out.println("The equation has two roots x1 = " + x1 + " and x2 = " + x2);
        } else if (discriminant < -ADMISSIBLE_ERROR) {
            System.out.println("The equation hasn't roots");
        } else {
            double x1 = (-b / (2 * a));
            System.out.println("The equation has one root x1 = " + x1);
        }
    }

    //Reading param from console
    private static double readConsole() {
        Scanner in = new Scanner(System.in);
        String line = "";
        while (true) {
            line = in.nextLine();
            try {
                return Double.valueOf(line);
            } catch (Exception e) {
                if (line.equals("q")) {
                    System.exit(0);
                }
                System.out.println("Insert number for parameter or input q for exit");
            }
        }
    }
}